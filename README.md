# Website richys.gitlab.io

### ¿Que haces aquí mi Friend?

Si estas aquí, me imagino que es por que tienes curiosidad de que es esto, o como esta montado esto, te invito a que tengas mas curiosidad ;)

-- richys

## Pipeline Branch Master:


[![pipeline status](https://gitlab.com/richys/richys.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/richys/richys.gitlab.io/commits/master)


## Pipeline Branch Test:

[![pipeline status](https://gitlab.com/richys/richys.gitlab.io/badges/test/pipeline.svg)](https://gitlab.com/richys/richys.gitlab.io/commits/test)


_Muestra el estado de los Pipeline, de ambos Branch "Cadenas" que tengo, en esta caso dos; la Cadena **master** y la cadena de **test**._

### EOF