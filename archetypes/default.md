---
title: "{{ replace .TranslationBaseName '-' ' ' | title }}"
description: ""
date: "{{ .Date }}"
thumbnail: "img/headers/NOMBRE" #Poner imagen de cabecera
draft: true #Estado Borrador cambiar a false para subir a la web
sidebar: true #Habilitado para mostrar barra derecha
categories:
  - ""
tags:
  - ""
menu: #Poner menu si es necesario, pero no lo creo
authorbox: true #No aparece al final el usuario
---
