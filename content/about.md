---
title: "About"
sidebar: true #Habilitado para mostrar barra derecha
menu: main #Poner menu si es necesario, pero no lo creo
authorbox: false #No aparece al final el usuario
weight: 2
---

Hola, 

Mi nombre es José Ricardo _"RICHYS"_, Soy Administrador de Sistemas y Entusiasta de GNU/Linux y de todo lo que tenga que ver con el Software Libre, y nuevas tecnologías.

Tengo estudios en Administración de sistemas y Redes, tanto en <abbr title="Sistemas Operativos">SO</abbr> Windows como GNU/Linux, en Servidores como en Desktop. Investigador Nato.

Gracias a todos los que me están leyendo, o por curiosidad Habéis entrado aquí &#10084;, este sitio mas que un blog es un _"Portafolio"_, lo que se compartiré aqui sera lo de mi dia a dia en la _vida sysadmin_ "Resolución de problemas típicos, How to, Trucos and TIC", y de paso me servirá como guiá para mi mismo.

Este blog/Portafolio funciona gracias al Software Libre, gracia a las herramientas:

* [Hugo](https://gohugo.io/)
* [GitLab](https://gitlab.com/)
* [Git](https://git-scm.com/)
* [MarkDown](https://www.markdownguide.org/)

Para la iniciación de este proyecto estoy probando el mundo de las [GitLabPages](https://docs.gitlab.com/ee/user/project/pages/), en un futuro no muy lejano _Esperemos_, migrare todo el proyecto a un _<abbr title="Servidor Privador Virtual">VPS</abbr>_, pero bueno por el momento estamos alojados en GitLab, que por el momento funciona de maravilla sin problemas.

Para el tema de contacto tenéis mis _<abbr title="Redes Sociales">RRSS</abbr>_, en el apartado social del blog, para darme algún _Feedback_.

-- _Just For Fun &#10084;_ 


