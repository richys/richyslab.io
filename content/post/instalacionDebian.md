---
title: "Instalación del SO Debian"
description: "Install party de Debian"
date: "2019-10-17T01:51:40+02:00"
thumbnail: "/img/post/debian-logo.jpg" #Poner imagen de cabecera
draft: true #Estado Borrador cambiar a false para subir a la web
sidebar: true #Habilitado para mostrar barra derecha
categories:
  - "Sistemas"
  - "GNU_Linux"
tags:
  - "How To"
---

Hola, en este capitulo veremos como instalar la Mejor Distribución desde mi punto de vista, **Debian** la version 10.
<!--more-->

### Instalación básica de Debian 10

Lo primero que haremos es es ir a la pagina web del Proyecto Debian:

[Ir al proyecto](https://www.debian.org/CD/http-ftp/ "Pagina del proyecto debian")

![this is a picture](/img/post/a1.png "Imagen Opciones a descargar")

Como podemos ver existen muchas versiones, en esta caso instalaremos la version Net-install, este version requiere internet para su instalación, lo cual nos permitirá tener un versión Actualizada:

[Download](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/)

![this is a picture](/img/post/a2.png "Imagen Opciones a descargar")

### Pasos para la descargar y comprobación

Lo que tenemos que hacer es abrir nuestra terminal y nos colocamos por ejemplo en

`cd ~/Descargas/` y pegamos los siguientes links:

* Descargar la Imagen ISO:

`wget https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.1.0-amd64-netinst.iso`

![this is a picture](/img/post/ "")

* Descargar el Archivo CheckSUM MD5 en esta caso:

`wget https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/MD5SUMS`

![this is a picture](/img/post/ "")

* Para comprobar el resultado del checksum

`md5sum -c MD5SUM`

![this is a picture](/img/post/ "")


> Esto de la comprobación de checksum es un _"norma"_ básica de seguridad, en la cual comparamos la integridad de nuestra Imagen ISO, se puedo obviar si confiamos en el emisor *PANIC*.

**Otras Versiones no NET-INSTALL**

[Download](https://www.debian.org/CD/http-ftp/)

### Creación del USB Bootloader 

Una vez que ya tenemos descarada nuestra Imagen ISO y hemos hecho la comprobación correspondiente, ahora pasaremos a crear un memoria USB "Booteable", este contendrá nuestro sistema Operativo Debian.

Para esto usaremos el comando `dd` también se podría usar un programa de gráfico, hay que tener en cuenta que tendremos que saber cuantos discos tenemos en nuestro PC, en los sistemas GNU/Linux los disco de nombran de la siguiente manera: ***sda, sdb, sdc*** siendo sda el disco 1, y sdb el 2, etc...

Para ver discos tenemos podemos usar el comando `lsblk` este comandos nos dará información sobres los discos y particiones que tenemos, como podemos ver yo tengo 2 discos: `sda` que es del SO y `sdb` que es mi Memoria USB; una vez ya tengamos localizados nuestros discos ya podremos continuar.

```bash 
richys@debiana:~$ lsblk 
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda      8:0    0   20G  0 disk 
├─sda1   8:1    0   18G  0 part /
├─sda2   8:2    0    1K  0 part 
└─sda5   8:5    0    2G  0 part [SWAP]
sdb      8:16   0    8G  0 disk 
sr0     11:0    1 1024M  0 rom  
```
Ahora que ya sabemos los discos que tenemos, sabremos que nuestra memoria USB va hacer el disco `sdb` procedemos con la ejecución del comando de la siguiente manera.

**Sintaxis**

* ***if:*** Archivo de IMAGEN ISO.
* ***of:*** Ruta del USB, siempre empieza por /dev/sdX 
* ***status=progress:*** nos permite ver el _log_ en todo momento sobre el proceso.
* ***conv=sync:*** nos permite sincronizar todo nuestros natos y hace la función de _"Quitar USB de Forma Segura"_

`dd if=/debian-10.iso of=/dev/sdb status=progress conv=sync`

**Ejemplo aplicado**

```bash
# dd if=debian-10.1.0-amd64-netinst.iso of=/dev/sdb status=progress conv=sync

350515712 bytes (351 MB, 334 MiB) copied, 88,0003 s, 4,0 MB/s
686080+0 registros leídos
686080+0 registros escritos
351272960 bytes (351 MB, 335 MiB) copied, 88,1228 s, 4,0 MB/s
```
Ahora, lo que tenemos que hacer es instalarlo en nuestro PC, en este tutorial se mostrara los paso su instalación en un entorno virtual.

### Empezamos

que este el PC conectado a internet

### Pasos 



